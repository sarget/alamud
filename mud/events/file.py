from .event import Event3
import mud.game

class FileEvent(Event3):
	NAME = "file"
	
	def perform(self):
		if not(self.object2.has_prop("boite")):
			self.add_prop("object2_not_a_container")
			return self.file_failed()
		if self.object.is_empty():
			self.add_prop("pokevide")
			return self.file_failed()
		for elem in self.object.contents():
			elem.add_prop("depose")
			self.object2.add(elem)
			self.object.remove(elem)
			break
		if (self.object2.longueur()>=1 and mud.game.GAME.world["pkm-007"] in self.object2):
			if (mud.game.GAME.world["pkm-008"] not in self.object2):
				mud.game.GAME.world["pkm-008"].move_to(mud.game.GAME.world["Ocean-000"])
			if (mud.game.GAME.world["pkm-009"] not in self.object2):
				mud.game.GAME.world["pkm-009"].move_to(mud.game.GAME.world["Ocean-000"])
		#mud.game.GAME.world renvoie donc un dictionnaire dont les clés sont les id dans l'initial du yml.
		self.object.remove_prop("full")
		self.object.add_prop("catchposs")
		self.inform("file")
		
	def file_failed(self):
		self.fail()
		self.inform("file.failed")
