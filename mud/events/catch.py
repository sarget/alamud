from .event import Event3
from ..models.pokemon import Pokemon

class CatchEvent(Event3):
	NAME = "catch"

	def perform(self):
		if not type(self.object)==Pokemon:
			self.add_prop("object-not-takable")
			return self.catch_failed()
		if self.object2.has_prop("full"):
			self.add_prop("pokeball_is_full")
			return self.catch_failed()
		if not self.object.has_prop("catchable"):
			self.add_prop("not_catchable")
			return self.catch_failed()
		if self.object.has_prop("depose"):
			self.add_prop("depose")
			return self.catch_failed()
		self.object.move_to(self.object2)
		self.object.remove_prop("catchable")
		self.object2.remove_prop("catchposs")
		self.object2.add_prop("full")
		self.inform("catch")

	def catch_failed(self):
		self.fail()
		self.inform("catch.failed")
