from .event import Event1
import mud.game
from ..models.pokemon import Pokemon

class CheatEvent(Event1):
	NAME = "cheat"
	def perform(self):
		boite=mud.game.GAME.world["stockage-001"]
		for elem in mud.game.GAME.world.keys():
			if type(mud.game.GAME.world[elem])==Pokemon:
				if mud.game.GAME.world[elem] not in boite:
					mud.game.GAME.world[elem].remove_prop("catchable")
					mud.game.GAME.world[elem].move_to(boite)
		self.inform("cheat")
		
	def cheat_failed(self):
		self.fail()
		self.inform("cheat.failed")
