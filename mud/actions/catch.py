from .action import Action3
from mud.events import CatchEvent

class CatchAction(Action3):
    EVENT = CatchEvent
    RESOLVE_OBJECT = "resolve_for_catch"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "catch"
